Title: git.exherbo.org will be removed in favour of Gitlab
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2023-02-17
Revision: 1
News-Item-Format: 1.0

Due to the complexity involved in syncing between git.exherbo.org and
gitlab.exherbo.org and the little gain we get from it, we will be moving our
git repositories to gitlab.exherb.org completely.
This also means the end of the unencrypted git:// protocol, which offers no
integrity checking or authentication.

Depending on the age of your installation (or your configuration) you may have
varying kinds of sync URLs configured. You have to change any forms of URLs
containing "(git|http|https)://git.exherbo.org/" in your repository config
files under (usually) /etc/paludis/repositories/*.conf to point to the
respective repository on https://gitlab.exherbo.org.
There's a script in app-admin/change-git-exherbo-org-to-gitlab-exherb-org to
do that automagically for you:

$ change-git-exherbo-org-to-gitlab-exherb-org.py --dryrun

to see what it would change first and

$ change-git-exherbo-org-to-gitlab-exherb-org.py

to actually change the files.

For http:// and https:// URLs we added a redirect to the respective repository
on https://gitlab.exherbo.org/. But you should still update these URLs, too.
Not only to avoid a warning similar like this one:
"warning: redirecting to https://gitlab.exherbo.org/exherbo/arbor.git/".

We will keep around the old git server at git.exherbo.org for some time to
get this news item distributed to users who don't sync that often. But
replicating commits from gitlab.exherbo.org to git.exherbo.org will be shut
down soon, so you will *not* get new commits after this point, if you still
have git:// URLs configured.
You don't need to change the URL of unavailable and unavailable-unofficial
though, because they are not git repositories and the generated tarballs will
stay on git.exherbo.org for now.

Last but not least, if you're a developer, you need to update your remotes in
your local git clones, too. Pushing via ssh://git.exherbo.org/foo.git will not
be redirected. You can use git remote set-url to do update them.

