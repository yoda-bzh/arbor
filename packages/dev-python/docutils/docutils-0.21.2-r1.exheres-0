# Copyright 2008, 2009 Ali Polatel
# Copyright 2013 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'docutils-0.4-r3.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

local binaries=( docutils rst2html rst2html4 rst2html5 rst2latex rst2man rst2odt
                 rst2pseudoxml rst2s5 rst2xetex rst2xml )

require pypi py-pep517 [ backend=flit_core backend_version_spec="[>=3.4&<4]" blacklist='3.8' entrypoints=[ "${binaries[@]}" ] ]

SUMMARY="Set of tools for processing plaintext docs into HTML, XML, etc..."
DESCRIPTION="
Docutils is an open-source text processing system for processing plaintext documentation into
useful formats, such as HTML or LaTeX. It includes reStructuredText, the easy to read, easy to use,
what-you-see-is-what-you-get plaintext markup language.
"
HOMEPAGE+=" https://docutils.sourceforge.io"

REMOTE_IDS+=" sourceforge:docutils"

UPSTREAM_CHANGELOG="https://docutils.sourceforge.io/HISTORY.html"
UPSTREAM_RELEASE_NOTES="https://docutils.sourceforge.io/RELEASE-NOTES.html"

LICENCES="public-domain PYTHON BSD-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    suggestion:
        dev-python/Pillow [[ description = [ Support for images ] ]]
        dev-python/Pygments [[ description = [ Support syntax highlighting within code directive ] ]]
"

install_one_multibuild() {
    # Tools + Docs
    dobin tools/*.py
    dodoc -r docs

    py-pep517_install_one_multibuild
    SHEBANG=python${MULTIBUILD_TARGET}
}

src_install() {
    py-pep517_src_install

    edo sed \
        -e "1 s/python3$/${SHEBANG}/" \
        -i "${IMAGE}"/usr/$(exhost --target)/bin/*

    local missing_binaries=( buildhtml docutils-cli rst2odt_prepstyles rstpep2html )
    for bin in ${missing_binaries[@]}; do
        dosym "${bin}".py /usr/$(exhost --target)/bin/"${bin}"
    done
}

test_one_multibuild() {
    PYTHONPATH="$(ls -d build/lib*)" edo ${PYTHON} test/alltests.py
}

