From 4c2029b0e9f4aa5935bbd641bea1bf857380ac02 Mon Sep 17 00:00:00 2001
From: Marvin Schmidt <marv@exherbo.org>
Date: Fri, 1 May 2020 08:31:03 +0200
Subject: [PATCH] compiler-rt: Fix build on musl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Original patch is from Ali Polatel, just made it into a git format-patch
and rebased

Source: Ali Polatel <alip@exherbo.org>
Upstream:
        No.¹
        Codewise, this is mostly ready.
        Glibc prereq macros could use some tweaking.
Reason:
        1. Missing headers, functions et al. w\ musl.
           Upstream seems to assume linux == glibc...
        2. The initial hunk fixes missing _Unwind_Word define.
           I am not completely sure where it's supposed to originate from.
           Neither libunwind (1.2.0, 1.3.1-r1) nor llvm-unwind (9.0.1-r1, 10.0.0)
           defines it via <unwind.h> as far as I can tell.
           Hence I made an educated guess which seems to work, you've been warned.
        Note, ² is not necessarily related to musl.
License:
        Added/modified hunks are GPL-2 with no exception:
                Copyright 2020 Ali Polatel <alip@exherbo.org>
                Distributed under the terms of the GNU General Public License v2
        See llvm-project exlib for the rest.
---
 compiler-rt/lib/asan/asan_linux.cpp           |  4 +-
 compiler-rt/lib/hwasan/hwasan_exceptions.cpp  |  4 ++
 .../lib/interception/interception_linux.cpp   |  4 +-
 .../sanitizer_platform_limits_posix.cpp       | 54 +++++++++++++++----
 .../sanitizer_platform_limits_posix.h         | 23 ++++++--
 5 files changed, 71 insertions(+), 18 deletions(-)

diff --git a/compiler-rt/lib/asan/asan_linux.cpp b/compiler-rt/lib/asan/asan_linux.cpp
index ce5e873dc518..177ec0ed88b7 100644
--- a/compiler-rt/lib/asan/asan_linux.cpp
+++ b/compiler-rt/lib/asan/asan_linux.cpp
@@ -45,7 +45,7 @@
 #include <link.h>
 #endif
 
-#if SANITIZER_ANDROID || SANITIZER_FREEBSD || SANITIZER_SOLARIS
+#if SANITIZER_ANDROID || SANITIZER_FREEBSD || SANITIZER_SOLARIS || (SANITIZER_LINUX && !defined(__GLIBC__))
 #include <ucontext.h>
 extern "C" void* _DYNAMIC;
 #elif SANITIZER_NETBSD
@@ -138,7 +138,7 @@ void AsanApplyToGlobals(globals_op_fptr op, const void *needle) {
   UNIMPLEMENTED();
 }
 
-#if SANITIZER_ANDROID
+#if SANITIZER_ANDROID || (SANITIZER_LINUX && !defined(__GLIBC__))
 // FIXME: should we do anything for Android?
 void AsanCheckDynamicRTPrereqs() {}
 void AsanCheckIncompatibleRT() {}
diff --git a/compiler-rt/lib/hwasan/hwasan_exceptions.cpp b/compiler-rt/lib/hwasan/hwasan_exceptions.cpp
index 169e7876cb58..da96e362781d 100644
--- a/compiler-rt/lib/hwasan/hwasan_exceptions.cpp
+++ b/compiler-rt/lib/hwasan/hwasan_exceptions.cpp
@@ -16,6 +16,10 @@
 
 #include <unwind.h>
 
+#ifndef _Unwind_Word
+# define _Unwind_Word uintptr_t
+#endif
+
 using namespace __hwasan;
 using namespace __sanitizer;
 
diff --git a/compiler-rt/lib/interception/interception_linux.cpp b/compiler-rt/lib/interception/interception_linux.cpp
index 950cd5126538..f4942e239410 100644
--- a/compiler-rt/lib/interception/interception_linux.cpp
+++ b/compiler-rt/lib/interception/interception_linux.cpp
@@ -63,8 +63,8 @@ bool InterceptFunction(const char *name, uptr *ptr_to_real, uptr func,
   return addr && (func == wrapper);
 }
 
-// Android and Solaris do not have dlvsym
-#if !SANITIZER_ANDROID && !SANITIZER_SOLARIS && !SANITIZER_OPENBSD
+// Android and Solaris and musl-libc do not have dlvsym
+#if !SANITIZER_ANDROID && !SANITIZER_SOLARIS && !SANITIZER_OPENBSD && (!SANITIZER_LINUX || defined(__GLIBC__))
 static void *GetFuncAddr(const char *name, const char *ver) {
   return dlvsym(RTLD_NEXT, name, ver);
 }
diff --git a/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.cpp b/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.cpp
index 539e64e1d628..aa83ff88241a 100644
--- a/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.cpp
+++ b/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.cpp
@@ -26,6 +26,7 @@
 // Must go after undef _FILE_OFFSET_BITS.
 #include "sanitizer_glibc_version.h"
 
+#include <stdio.h> /* BUFSIZ on musl */
 #include <arpa/inet.h>
 #include <dirent.h>
 #include <grp.h>
@@ -58,7 +59,9 @@
 #endif
 
 #if !SANITIZER_ANDROID
-#include <fstab.h>
+# if defined(__GLIBC__)
+# include <fstab.h>
+# endif
 #include <sys/mount.h>
 #include <sys/timeb.h>
 #include <utmpx.h>
@@ -113,10 +116,17 @@ typedef struct user_fpregs elf_fpregset_t;
 #include <glob.h>
 #include <obstack.h>
 #include <mqueue.h>
-#include <net/if_ppp.h>
-#include <netax25/ax25.h>
-#include <netipx/ipx.h>
-#include <netrom/netrom.h>
+# if __GLIBC_PREREQ(2, 14)
+#  include <net/if_ppp.h>
+#  include <netax25/ax25.h>
+#  include <netipx/ipx.h>
+#  include <netrom/netrom.h>
+# else
+#  include <linux/if_ppp.h>
+#  include <linux/ax25.h>
+#  include <linux/ipx.h>
+#  include <linux/netrom.h>
+# endif
 #if HAVE_RPC_XDR_H
 # include <rpc/xdr.h>
 #endif
@@ -201,7 +211,11 @@ namespace __sanitizer {
 #endif // (SANITIZER_MAC && !TARGET_CPU_ARM64) && !SANITIZER_IOS
 
 #if !SANITIZER_ANDROID
+# if defined(__GLIBC__)
   unsigned struct_fstab_sz = sizeof(struct fstab);
+# else
+  unsigned struct_fstab_sz = 0;
+# endif
   unsigned struct_statfs_sz = sizeof(struct statfs);
   unsigned struct_sockaddr_sz = sizeof(struct sockaddr);
   unsigned ucontext_t_sz = sizeof(ucontext_t);
@@ -297,7 +311,7 @@ unsigned struct_ElfW_Phdr_sz = sizeof(ElfW(Phdr));
 unsigned struct_ElfW_Phdr_sz = sizeof(Elf_Phdr);
 #endif
 
-#if SANITIZER_LINUX && !SANITIZER_ANDROID
+#if SANITIZER_LINUX && !SANITIZER_ANDROID && defined(__GLIBC__)
   int glob_nomatch = GLOB_NOMATCH;
   int glob_altdirfunc = GLOB_ALTDIRFUNC;
 #endif
@@ -416,7 +430,11 @@ unsigned struct_ElfW_Phdr_sz = sizeof(Elf_Phdr);
   unsigned struct_input_id_sz = sizeof(struct input_id);
   unsigned struct_mtpos_sz = sizeof(struct mtpos);
   unsigned struct_rtentry_sz = sizeof(struct rtentry);
+# if defined(__GLIBC__)
   unsigned struct_termio_sz = sizeof(struct termio);
+# else
+  unsigned struct_termio_sz = 0;
+# endif
   unsigned struct_vt_consize_sz = sizeof(struct vt_consize);
   unsigned struct_vt_sizes_sz = sizeof(struct vt_sizes);
   unsigned struct_vt_stat_sz = sizeof(struct vt_stat);
@@ -442,7 +460,11 @@ unsigned struct_ElfW_Phdr_sz = sizeof(Elf_Phdr);
 #endif // SANITIZER_LINUX
 
 #if SANITIZER_LINUX && !SANITIZER_ANDROID
+# if __GLIBC_PREREQ(2, 14)
   unsigned struct_ax25_parms_struct_sz = sizeof(struct ax25_parms_struct);
+# else
+  unsigned struct_ax25_parms_struct_sz = 0;
+# endif
 #if EV_VERSION > (0x010000)
   unsigned struct_input_keymap_entry_sz = sizeof(struct input_keymap_entry);
 #else
@@ -454,7 +476,11 @@ unsigned struct_ElfW_Phdr_sz = sizeof(Elf_Phdr);
   unsigned struct_kbkeycode_sz = sizeof(struct kbkeycode);
   unsigned struct_kbsentry_sz = sizeof(struct kbsentry);
   unsigned struct_mtconfiginfo_sz = sizeof(struct mtconfiginfo);
+# if __GLIBC_PREREQ(2, 14)
   unsigned struct_nr_parms_struct_sz = sizeof(struct nr_parms_struct);
+# else
+  unsigned struct_nr_parms_struct_sz = 0;
+# endif
   unsigned struct_scc_modem_sz = sizeof(struct scc_modem);
   unsigned struct_scc_stat_sz = sizeof(struct scc_stat);
   unsigned struct_serial_multiport_struct_sz
@@ -873,16 +899,22 @@ unsigned struct_ElfW_Phdr_sz = sizeof(Elf_Phdr);
   unsigned IOCTL_SIOCAIPXPRISLT = SIOCAIPXPRISLT;
   unsigned IOCTL_SIOCAX25ADDUID = SIOCAX25ADDUID;
   unsigned IOCTL_SIOCAX25DELUID = SIOCAX25DELUID;
+# if __GLIBC_PREREQ(2, 14)
   unsigned IOCTL_SIOCAX25GETPARMS = SIOCAX25GETPARMS;
+# endif
   unsigned IOCTL_SIOCAX25GETUID = SIOCAX25GETUID;
   unsigned IOCTL_SIOCAX25NOUID = SIOCAX25NOUID;
+# if __GLIBC_PREREQ(2, 14)
   unsigned IOCTL_SIOCAX25SETPARMS = SIOCAX25SETPARMS;
+# endif
   unsigned IOCTL_SIOCDEVPLIP = SIOCDEVPLIP;
   unsigned IOCTL_SIOCIPXCFGDATA = SIOCIPXCFGDATA;
   unsigned IOCTL_SIOCNRDECOBS = SIOCNRDECOBS;
+# if __GLIBC_PREREQ(2, 14)
   unsigned IOCTL_SIOCNRGETPARMS = SIOCNRGETPARMS;
   unsigned IOCTL_SIOCNRRTCTL = SIOCNRRTCTL;
   unsigned IOCTL_SIOCNRSETPARMS = SIOCNRSETPARMS;
+# endif
   unsigned IOCTL_TIOCGSERIAL = TIOCGSERIAL;
   unsigned IOCTL_TIOCSERGETMULTI = TIOCSERGETMULTI;
   unsigned IOCTL_TIOCSERSETMULTI = TIOCSERSETMULTI;
@@ -953,7 +985,7 @@ CHECK_SIZE_AND_OFFSET(dl_phdr_info, dlpi_phdr);
 CHECK_SIZE_AND_OFFSET(dl_phdr_info, dlpi_phnum);
 #endif // SANITIZER_LINUX || SANITIZER_FREEBSD
 
-#if (SANITIZER_LINUX || SANITIZER_FREEBSD) && !SANITIZER_ANDROID
+#if ((SANITIZER_LINUX && defined(__GLIBC__)) || SANITIZER_FREEBSD) && !SANITIZER_ANDROID
 CHECK_TYPE_SIZE(glob_t);
 CHECK_SIZE_AND_OFFSET(glob_t, gl_pathc);
 CHECK_SIZE_AND_OFFSET(glob_t, gl_pathv);
@@ -987,6 +1019,7 @@ CHECK_TYPE_SIZE(iovec);
 CHECK_SIZE_AND_OFFSET(iovec, iov_base);
 CHECK_SIZE_AND_OFFSET(iovec, iov_len);
 
+#if SANITIZER_LINUX && __GLIBC_PREREQ(2, 14)
 CHECK_TYPE_SIZE(msghdr);
 CHECK_SIZE_AND_OFFSET(msghdr, msg_name);
 CHECK_SIZE_AND_OFFSET(msghdr, msg_namelen);
@@ -1000,6 +1033,7 @@ CHECK_TYPE_SIZE(cmsghdr);
 CHECK_SIZE_AND_OFFSET(cmsghdr, cmsg_len);
 CHECK_SIZE_AND_OFFSET(cmsghdr, cmsg_level);
 CHECK_SIZE_AND_OFFSET(cmsghdr, cmsg_type);
+#endif
 
 #if SANITIZER_LINUX && (__ANDROID_API__ >= 21 || __GLIBC_PREREQ (2, 14))
 CHECK_TYPE_SIZE(mmsghdr);
@@ -1105,7 +1139,7 @@ CHECK_SIZE_AND_OFFSET(mntent, mnt_passno);
 
 CHECK_TYPE_SIZE(ether_addr);
 
-#if (SANITIZER_LINUX || SANITIZER_FREEBSD) && !SANITIZER_ANDROID
+#if ((SANITIZER_LINUX && __GLIBC_PREREQ(2, 14)) || SANITIZER_FREEBSD) && !SANITIZER_ANDROID
 CHECK_TYPE_SIZE(ipc_perm);
 # if SANITIZER_FREEBSD
 CHECK_SIZE_AND_OFFSET(ipc_perm, key);
@@ -1167,7 +1201,7 @@ CHECK_SIZE_AND_OFFSET(ifaddrs, ifa_dstaddr);
 CHECK_SIZE_AND_OFFSET(ifaddrs, ifa_data);
 #endif
 
-#if SANITIZER_LINUX
+#if SANITIZER_LINUX && __GLIBC_PREREQ(2, 14)
 COMPILER_CHECK(sizeof(__sanitizer_struct_mallinfo) == sizeof(struct mallinfo));
 #endif
 
@@ -1217,7 +1251,7 @@ COMPILER_CHECK(__sanitizer_XDR_DECODE == XDR_DECODE);
 COMPILER_CHECK(__sanitizer_XDR_FREE == XDR_FREE);
 #endif
 
-#if SANITIZER_LINUX && !SANITIZER_ANDROID
+#if SANITIZER_LINUX && !SANITIZER_ANDROID && __GLIBC_PREREQ(2, 14)
 COMPILER_CHECK(sizeof(__sanitizer_FILE) <= sizeof(FILE));
 CHECK_SIZE_AND_OFFSET(FILE, _flags);
 CHECK_SIZE_AND_OFFSET(FILE, _IO_read_ptr);
diff --git a/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.h b/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.h
index 85d024191722..1383278d6a28 100644
--- a/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.h
+++ b/compiler-rt/lib/sanitizer_common/sanitizer_platform_limits_posix.h
@@ -24,6 +24,11 @@
 #include "sanitizer_glibc_version.h"
 #endif
 
+# ifndef __GLIBC_PREREQ
+/* Avoid, error: function-like macro '__GLIBC_PREREQ' not defined. */
+#  define __GLIBC_PREREQ(...) (0)
+# endif
+
 # define GET_LINK_MAP_BY_DLOPEN_HANDLE(handle) ((link_map*)(handle))
 
 namespace __sanitizer {
@@ -54,7 +59,9 @@ extern unsigned struct_regex_sz;
 extern unsigned struct_regmatch_sz;
 
 #if !SANITIZER_ANDROID
+# if defined(__GLIBC__)
 extern unsigned struct_fstab_sz;
+# endif
 extern unsigned struct_statfs_sz;
 extern unsigned struct_sockaddr_sz;
 extern unsigned ucontext_t_sz;
@@ -761,12 +768,10 @@ struct __sanitizer_glob_t {
   int (*gl_lstat)(const char *, void *);
   int (*gl_stat)(const char *, void *);
 };
-# endif  // SANITIZER_LINUX
 
-# if SANITIZER_LINUX
 extern int glob_nomatch;
 extern int glob_altdirfunc;
-# endif
+# endif  // SANITIZER_LINUX
 #endif  // !SANITIZER_ANDROID
 
 extern unsigned path_max;
@@ -777,7 +782,7 @@ struct __sanitizer_wordexp_t {
   uptr we_offs;
 };
 
-#if SANITIZER_LINUX && !SANITIZER_ANDROID
+#if SANITIZER_LINUX && !SANITIZER_ANDROID && __GLIBC_PREREQ(2, 14)
 struct __sanitizer_FILE {
   int _flags;
   char *_IO_read_ptr;
@@ -980,7 +985,9 @@ extern unsigned struct_vt_mode_sz;
 #endif // SANITIZER_LINUX
 
 #if SANITIZER_LINUX && !SANITIZER_ANDROID
+# if __GLIBC_PREREQ(2, 14)
 extern unsigned struct_ax25_parms_struct_sz;
+#endif
 extern unsigned struct_input_keymap_entry_sz;
 extern unsigned struct_ipx_config_data_sz;
 extern unsigned struct_kbdiacrs_sz;
@@ -988,7 +995,9 @@ extern unsigned struct_kbentry_sz;
 extern unsigned struct_kbkeycode_sz;
 extern unsigned struct_kbsentry_sz;
 extern unsigned struct_mtconfiginfo_sz;
+# if __GLIBC_PREREQ(2, 14)
 extern unsigned struct_nr_parms_struct_sz;
+# endif
 extern unsigned struct_scc_modem_sz;
 extern unsigned struct_scc_stat_sz;
 extern unsigned struct_serial_multiport_struct_sz;
@@ -1384,16 +1393,22 @@ extern unsigned IOCTL_SIOCAIPXITFCRT;
 extern unsigned IOCTL_SIOCAIPXPRISLT;
 extern unsigned IOCTL_SIOCAX25ADDUID;
 extern unsigned IOCTL_SIOCAX25DELUID;
+# if __GLIBC_PREREQ(2, 14)
 extern unsigned IOCTL_SIOCAX25GETPARMS;
+# endif
 extern unsigned IOCTL_SIOCAX25GETUID;
 extern unsigned IOCTL_SIOCAX25NOUID;
+# if __GLIBC_PREREQ(2, 14)
 extern unsigned IOCTL_SIOCAX25SETPARMS;
+# endif
 extern unsigned IOCTL_SIOCDEVPLIP;
 extern unsigned IOCTL_SIOCIPXCFGDATA;
 extern unsigned IOCTL_SIOCNRDECOBS;
+# if __GLIBC_PREREQ(2, 14)
 extern unsigned IOCTL_SIOCNRGETPARMS;
 extern unsigned IOCTL_SIOCNRRTCTL;
 extern unsigned IOCTL_SIOCNRSETPARMS;
+# endif
 extern unsigned IOCTL_SNDCTL_DSP_GETISPACE;
 extern unsigned IOCTL_SNDCTL_DSP_GETOSPACE;
 extern unsigned IOCTL_TIOCGSERIAL;
-- 
2.34.1

