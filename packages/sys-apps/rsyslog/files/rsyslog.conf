# /etc/rsyslog.conf
#
# This configuration is based on RainerScript, the new recommended syntax
# for RSYSLOG. See http://www.rsyslog.com/doc/rainerscript.html for further
# details.
#
# But if you don't want to learn something new at moment, don't worry: The
# legacy syntax is still supported.
#
# You may want to use the new RSYSLOG configuration builder to create your
# own more advanced configuration: http://www.rsyslog.com/rsyslog-configuration-builder/

# Check config syntax on startup and abort if unclean (default: off)
#$AbortOnUncleanConfig on


###############
### MODULES ###
###############

# Read syslog messages from default Unix socket /dev/log (e.g. via logger command)
module(load="imuxsock")

# Read messages from the kernel log and submits them to the syslog engine
module(load="imklog")

# Inject "--MARK--" messages every $Interval (seconds)
#module(load="immark" Interval="600")

# Provides UDP syslog reception
# for parameters see http://www.rsyslog.com/doc/imudp.html
#module(load="imudp")
#input(type="imudp" port="514")

# Provides TCP syslog reception
# for parameters see http://www.rsyslog.com/doc/imtcp.html
#module(load="imtcp")
#input(type="imtcp" port="514")


#########################
### GLOBAL DIRECTIVES ###
#########################

# Where to place auxiliary files
$WorkDirectory /var/lib/rsyslog

# Reduce repeating messages (default: off)
#$RepeatedMsgReduction on

# Set defaults for every output file
$Umask 0022

# Use default timestamp format
module(
	load="builtin:omfile"
	Template="RSYSLOG_TraditionalFileFormat"
	FileCreateMode="0644"
	DirCreateMode="0755"
)


# Include all conf files in /etc/rsyslog.d/
$IncludeConfig /etc/rsyslog.d/*.conf
