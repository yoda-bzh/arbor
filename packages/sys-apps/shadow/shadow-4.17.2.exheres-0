# Copyright 2008-2012 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'shadow-4.1.2-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require github [ user=shadow-maint release=${PV} suffix=tar.xz ] \
    pam \
    option-renames [ renames=[ 'attr xattr' ] ]

SUMMARY="User and group management related utilities"

LICENCES="|| ( BSD-3 GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    acl   [[ description = [ Add support for ACL when copying user directories ] ]]
    btrfs [[ description = [ Add support for btrfs subvolumes for user homes ] ]]
    caps
    sssd  [[ description = [ Add support for flushing sssd caches ] ]]
    xattr [[ description = [ Add support for extended attributes when copying user directories ] ]]

    ( libc: musl )
    ( linguas: bs ca cs da de dz el es eu fi fr gl he hu id it ja ka kk km ko nb ne nl nn pl pt
               pt_BR ro ru sk sq sv tl tr uk vi zh_CN zh_TW )
    ( providers: systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19]
        virtual/pkg-config
    build+run:
        sys-libs/pam[>=1.0.1]
        acl? ( sys-apps/acl )
        btrfs? ( sys-fs/btrfs-progs )
        caps? ( sys-libs/libcap )
        !libc:musl? (
            dev-libs/libxcrypt:=
            sys-libs/glibc[>=2.38]
        )
        providers:systemd? ( sys-apps/systemd )
        sssd? ( sys-auth/sssd )
        xattr? ( sys-apps/attr )
        !sys-apps/busybox[<1.36.1-r1] [[
            description = [ sys-apps/busybox 1.36.1-r1 installs group previously provided by this package ]
            resolution = uninstall-blocked-after
        ]]
        !sys-apps/coreutils[<9.5-r1] [[
            description = [ sys-apps/coreutils 9.5-r1 installs group previously provided by this package ]
            resolution = uninstall-blocked-after
        ]]
        !sys-apps/util-linux[<2.37.2-r2] [[
            description = [ sys-apps/util-linux 2.37.2-r2 installs su previously provided by this package ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-util/cmocka
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --bindir="/usr/$(exhost --target)/bin"
    --enable-lastlog
    --enable-nls
    --enable-shadowgrp
    --enable-shared
    --disable-static
    --with-group-name-max-length=1024
    --with-libpam
    --with-nscd
    --with-sha-crypt
    --with-yescrypt
    --without-audit
    --without-bcrypt
    --without-libbsd
    --without-selinux
    --without-su
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'providers:systemd logind'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    acl
    btrfs
    'caps fcaps'
    sssd
    'xattr attr'
)

DEFAULT_SRC_INSTALL_PARAMS=( suidperms=4711 ubindir='${bindir}' usbindir='${sbindir}' )
DEFAULT_SRC_INSTALL_EXTRA_PREFIXES="doc/"
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( HOWTO WISHLIST console.c.spec.txt )

src_prepare() {
    default

    edo sed -i \
        -e '/^LOGIN_RETRIES/   s:5$:3:'   \
        -e '/^#MD5_CRYPT_ENAB/ s:no:yes:' \
        -e '/^#MD5_CRYPT_ENAB/ s:^#::'    \
        etc/login.defs
}

src_install() {
    default

    # https://github.com/shadow-maint/shadow/issues/389
    emake DESTDIR="${IMAGE}" -C man install-man

    insinto /etc
    insopts -m0600
    doins "${FILES}"/securetty

    # needed for 'adduser -D'
    insinto /etc/default
    insopts -m0600 ; doins "${FILES}"/useradd

    insinto /etc
    insopts -m0644
    newins etc/login.defs login.defs

    dopamd "${FILES}"/pam.d-include/{passwd,shadow}
    newpamd "${FILES}"/login.pamd.2 login

    for x in passwd chpasswd chgpasswd; do
        newpamd "${FILES}"/pam.d-include/passwd ${x}
    done

    for x in chage chsh chfn newusers user{add,del,mod} group{add,del,mod} ; do
        newpamd "${FILES}"/pam.d-include/shadow ${x}
    done

    # comment out login.defs options that pam hates
    edo sed -i -f "${FILES}"/login_defs_pam.sed "${IMAGE}"/etc/login.defs

    # remove manpages that pam will install for us
    # and/or don't apply when using pam
    edo find "${IMAGE}"/usr/share/man '(' -name 'limits.5*' -o -name 'suauth.5*' ')' -delete

    # Remove manpages that are handled by other packages
    edo find "${IMAGE}"/usr/share/man '(' -name passwd.5 -o -name getspnam.3 -o -name nologin.8 -o -name su.1 ')' -delete

    # Remove nologin which is installed by util-linux[>=2.24]
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/nologin

    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    # Check for inconsistencies in passwd/shadow entries
    if ! pwck -r -q -R "${ROOT:-/}" &>/dev/null ; then
        eerror "Running 'pwck' returned errors. Please run it manually to fix any errors."
    fi

    # Enable shadow groups (we need ROOT=/ here, as grpconv only operates on / ...).
    if [[ ${ROOT} == / && ! -f /etc/gshadow ]] ; then
        if grpck -r ; then
            grpconv
        else
            eerror "Running 'grpck -r' returned errors. Please run it by hand, and then"
            eerror "run 'grpconv' afterwards."
        fi
    fi

    # Create /etc/sub{g,u}id, tools like usermod expect them to be present
    [[ ! -f "${ROOT}"/etc/subgid ]] && touch "${ROOT}"/etc/subgid
    [[ ! -f "${ROOT}"/etc/subuid ]] && touch "${ROOT}"/etc/subuid

    if option caps; then
        setcap cap_setuid+ep /usr/$(exhost --target)/bin/newuidmap
        setcap cap_setgid+ep /usr/$(exhost --target)/bin/newgidmap
    fi
}

