# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'emacs-22.2-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require option-renames [ renames=[ 'gtk providers:gtk2' 'gtk3 providers:gtk3' ] ]
require alternatives elisp-module
require test-dbus-daemon

if [[ -z $(ever range 3) ]]; then
    require gnu [ suffix=tar.xz ]
else
    require gnu [ alpha=true subdir=pretest suffix=tar.xz ]
fi

if [[ $(ever range 3) == rc* ]]; then
    MY_PV=$(ever range 1-2)
    MY_PNV=${PN}-${MY_PV}
fi

export_exlib_phases src_prepare src_configure src_test src_install pkg_postinst

SUMMARY="The extensible, customizable, self-documenting, real-time display editor"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    X acl alsa dbus freetype gif gnutls gpm harfbuzz lcms otf svg systemd tiff xim
    (
        cairo freetype gif harfbuzz otf svg tiff xim
        providers: gtk2 gtk3
    ) [[ requires = X ]]
    ( providers: gtk2 gtk3 ) [[ number-selected = at-most-one ]]
    acl [[
        description = [ Support for manipulating Access Control Lists ]
    ]]
    gconf [[
        description = [ Allows emacs to determine the system font ]
        requires = X
    ]]
    harfbuzz [[
        description = [ Support for the harfbuzz OpenType shaping library ]
        requires = freetype
    ]]
    lcms [[
        description = [ Support for the Little CMS color management system ]
        requires = X
    ]]
    modules [[ description = [ Allows emacs to load modules from other sources at runtime ] ]]
    otf [[
        description = [ Support for OpenType fonts ]
        requires = freetype
    ]]
    webp [[
        description = [ Support for the WebP image file format ]
        requires = X
    ]]
    xim [[
        description = [ Support for X Input Method ]
    ]]
    X? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        sys-apps/gawk
        X? ( virtual/pkg-config )
        alsa? ( virtual/pkg-config )
        dbus? ( virtual/pkg-config )
        svg? ( virtual/pkg-config )
        providers:gtk2? ( virtual/pkg-config )
        providers:gtk3? ( virtual/pkg-config )
    build+run:
        dev-libs/glib:2[>=2.28]
        dev-libs/gmp:=
        dev-libs/jansson[>=2.7]
        dev-libs/libxml2:2.0[>=2.6.17] [[ note = [ used for html rendering; could be made optional ] ]]
        net-libs/liblockfile
        sys-libs/ncurses
        sys-libs/zlib
        acl? ( sys-apps/acl )
        alsa? ( sys-sound/alsa-lib[>=1.0.0] )
        X? (
            media-libs/libpng:=
            x11-libs/libICE
            x11-libs/libXpm
            x11-libs/libSM
            x11-libs/libX11
            x11-libs/libXcomposite
            x11-libs/libXext
            x11-libs/libXfixes[>=4.0.0]
            x11-libs/libXi
            x11-libs/libXinerama
            x11-libs/libXrandr[>=1.2.2]
            x11-libs/libxcb
            x11-utils/xcb-util
            gif? ( media-libs/giflib:= )
            gnutls? ( dev-libs/gnutls[>=2.12.2] )
            lcms? ( media-libs/lcms2 )
            otf? ( dev-libs/libotf )
            svg? ( gnome-desktop/librsvg:2[>=2.14.0] )
            tiff? ( media-libs/tiff:= )
            providers:gtk2? (
                dev-libs/glib:2[>=2.28]
                x11-libs/gtk+:2[>=2.24]
            )
            providers:gtk3? (
                dev-libs/glib:2[>=2.37.5]
                x11-libs/gtk+:3[>=3.10]
            )
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
            webp? ( media-libs/libwebp:=[>=0.6.0] )
        )
        dbus? ( sys-apps/dbus[>=1.0.0] )
        freetype? (
            media-libs/fontconfig[>=2.2.0]
            media-libs/freetype:2
            cairo? ( x11-libs/cairo[>=1.8.0] ) [[ note = [ cairo depends on freetype ] ]]
            harfbuzz? ( x11-libs/harfbuzz[>=0.9.42] ) [[ note = [ harfbuzz does NOT support bitmap fonts ] ]]
            !cairo? (
                x11-libs/libXft[>=0.13.0] [[ note = [ fallback when cairo is NOT enabled ] ]]
                x11-libs/libXrender [[ note = [ dependency of libXft ] ]]
            )
        )
        gconf? ( gnome-platform/GConf:2 )
        gpm? ( sys-libs/gpm )
        systemd? ( sys-apps/systemd )
   suggestion:
        app-spell/aspell [[ description = [ For ispell and flyspell mode ] ]]
"

WORK=${WORKBASE}/${MY_PNV-${PNV}}

emacs_src_prepare() {
    # Compressing *.el files saves 25MB diskspace, which is not worth it on modern systems
    edo sed -i -e 's/@GZIP_PROG@//' {.,leim}/Makefile.in
    default

    # Create a copy of the clean source (it will be installed in src_install)
    edo tar cf "${WORKBASE}"/source.tar src/
}

emacs_src_configure() {
    config_params=(
        PKG_CONFIG=/usr/$(exhost --target)/bin/$(exhost --tool-prefix)pkg-config
        --infodir=/usr/share/info/emacs
        --with-file-notification=inotify
        --with-json
        --with-native-compilation=no
        --with-threads
        --with-xml2
        --with-zlib
        --without-be-app
        --without-be-cairo
        --without-cairo-xcb
        --without-cygwin32-native-compilation
        --without-gsettings
        --without-hesiod
        --without-imagemagick
        --without-kerberos
        --without-kerberos5
        --without-m17n-flt
        --without-native-image-api
        --without-pgtk
        --without-selinux
        --without-small-ja-dic
        --without-sqlite3
        --without-tree-sitter
        --without-xaw3d
        --without-xwidgets
        --without-wide-int
        --without-w32
    )

    config_withs=(
        'X x' 'X jpeg' 'X png' 'X xdbe' 'X xinput2' 'X xpm'
        'alsa sound'
        'freetype xft'
        'lcms lcms2'
        'otf libotf'
        'svg rsvg'
        'systemd libsystemd'
        'cairo' 'dbus' 'gconf' 'gif' 'gnutls'
        'gpm' 'harfbuzz' 'modules' 'tiff' 'xim'
        'webp'
    )

    local toolkit=no
    option providers:gtk2 && toolkit=gtk2;
    option providers:gtk3 && toolkit=gtk3;

    econf \
        "${config_params[@]}" \
        $(for s in "${config_withs[@]}" ; do option_with ${s} ; done ) \
        $(option providers:gtk2 gtk || option providers:gtk3 gtk3 || echo --without-toolkit-scroll-bars) \
        --with-x-toolkit=${toolkit} \
        $(option acl || echo --disable-acl) # --enable-acl is the default
}

# Failing tests in emacs-28.1 reported here:
# https://debbugs.gnu.org/cgi/bugreport.cgi?bug=57301
RESTRICT=test

emacs_src_test() {
    esandbox allow_net "unix:/tmp/server-test*"
    esandbox allow_net "unix:${TEMP}/pkg-test-user-dir-*/gnupg/S.gpg-agent*"
    esandbox allow_net "unix:${TEMP}/epg-tests-homedir*/S.gpg-agent*"
    esandbox allow_net "unix:${TEMP}/test-dir-*/server"
    esandbox allow_net --connect "unix:${TEMP}/pkg-test-user-dir-*/gnupg/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/epg-tests-homedir*/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/test-dir-*/server"

    test-dbus-daemon_run-tests

    esandbox disallow_net "unix:/tmp/server-test*"
    esandbox disallow_net "unix:${TEMP}/pkg-test-user-dir-*/gnupg/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/epg-tests-homedir*/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/test-dir-*/server"
    esandbox disallow_net --connect "unix:${TEMP}/pkg-test-user-dir-*/gnupg/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${TEMP}/epg-tests-homedir*/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${TEMP}/test-dir-*/server"
}

emacs_src_install() {
    default

    insinto ${ELISP_SITE_LISP}
    hereins site-start.el <<EOF
(require 'site-exherbo)
EOF
    # ' quote to satisfy editor highlighting

    elisp-install-site-file

    # Install a copy of the clean source to `source-directory`. The builtin help system of emacs
    # requires it in order to fully function.
    local emacs_source_root=/usr/src/emacs
    dodir ${emacs_source_root}
    edo tar xf "${WORKBASE}"/source.tar -C "${IMAGE}"${emacs_source_root}

    cd "${IMAGE}"
    edo mv usr/share/man/man1/ctags{,-emacs}.1
    edo mv usr/${CHOST}/bin/ctags{,-emacs}
    alternatives_for ctags emacs 500 \
        /usr/${CHOST}/bin/ctags ctags-emacs \
        /usr/share/man/man1/ctags.1 ctags-emacs.1

    # Do not install empty directory
    [[ -d "${IMAGE}"/usr/share/emacs/${PV}/lisp/emacs-parallel ]] && edo rmdir "${IMAGE}"/usr/share/emacs/${PV}/lisp/emacs-parallel

    hereenvd 70${PN} <<EOF
INFOPATH="/usr/share/info/${PN}"
EOF
}

emacs_pkg_postinst() {
    alternatives_pkg_postinst
    elisp-generate-global-site-file
}

